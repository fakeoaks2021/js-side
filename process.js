import { WASI } from '@wasmer/wasi';
import { WasmFs } from "@wasmer/wasmfs";
import { lowerI64Imports } from "@wasmer/wasm-transformer";
import browserBindings from "@wasmer/wasi/lib/bindings/browser";

const cleanStdout = stdout => {
    const pattern = [
        "[\\u001B\\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)",
        "(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-ntqry=><~]))"
    ].join("|");

    const regexPattern = new RegExp(pattern, "g");
    return stdout.replace(regexPattern, "");
};

class Process
{
    constructor(
        wasmFilePath,
        sharedStdinBuffer,
        dataCallback,
        startStdinReadCallback)
    {
        this.wasmFs = new WasmFs();

        this.wasmFilePath = wasmFilePath;
        this.dataCallback = dataCallback;
        this.wasmFs.volume.fds[0].node.read = this.stdinRead.bind(this);
        this.wasmFs.volume.fds[1].node.write = this.stdoutWrite.bind(this);
        this.wasmFs.volume.fds[2].node.write = this.stdoutWrite.bind(this);
        const ttyFd = this.wasmFs.volume.openSync("/dev/tty", "w+");
        this.wasmFs.volume.fds[ttyFd].node.read = this.stdinRead.bind(this);
        this.wasmFs.volume.fds[ttyFd].node.write = this.stdoutWrite.bind(this);

        this.sharedStdin = new Int32Array(sharedStdinBuffer);
        this.startStdinReadCallback = startStdinReadCallback;
        this.readStdinCounter = 0;
        this.lastReq = false;
    }

    async start()
    {
        let wasi = new WASI(
        {
            // Arguments to pass to the Wasm Module
            // The first argument usually should be the filepath to the "executable wasi module"
            // That we want to run.
            args: [this.wasmFilePath],
            env: {},
            bindings: {
            ...browserBindings,
                    fs: this.wasmFs.fs
            }
        });

        const response = await fetch(this.wasmFilePath);
        const responseArrayBuffer = await response.arrayBuffer();
        const wasmBytes = new Uint8Array(responseArrayBuffer);
        // Lower the WebAssembly Module bytes
        // This will create trampoline functions for i64 parameters
        // in function calls like:
        // https://github.com/WebAssembly/WASI/blob/master/phases/old/snapshot_0/docs/wasi_unstable.md#clock_time_get
        // Allowing the Wasi module to work in the browser / node!
        const loweredWasmBytes = await lowerI64Imports(wasmBytes);

        // Instantiate the WebAssembly file
        let wasmModule = await WebAssembly.compile(wasmBytes);
        let instance = await WebAssembly.instantiate(wasmModule, {
        ...wasi.getImports(wasmModule)
        });
            // Start the WebAssembly WASdI instance!
        try {
            wasi.start(instance);
        } catch(e) {
            // Catch errors, and if it is not a forced user error (User cancelled the prompt)
            // Log the error and end the process
            if (!e.user) {
                console.error(e);
                return;
            }
        }
    }

    stdoutWrite(
        stdoutBuffer,
        offset,
        length,
        position)
    {
        this.lastReq = this.dataCallback(stdoutBuffer, this);
        return stdoutBuffer.length;
    }

    stdinRead(
        stdinBuffer,
        offset,
        length,
        position)
    {
        let responseStdin = null;
        if (this.sharedStdin && this.startStdinReadCallback)
        {
            this.startStdinReadCallback();
            if (this.lastReq === true) {
                postMessage("answer");
                this.lastReq = false;
            } else {
                postMessage("notify");
            }

            Atomics.wait(this.sharedStdin, 0, -1);
            // Grab the of elements
            const numberOfElements = this.sharedStdin[0];
            const newStdinData = new Uint8Array(numberOfElements);
            for (let i = 0; i < numberOfElements; i++) {
                newStdinData[i] = this.sharedStdin[i + 1];
            }
            responseStdin = new TextDecoder("utf-8").decode(newStdinData);
            if (!responseStdin) {
                return 0;
            }

            Atomics.store(this.sharedStdin, 0, -1);
            Atomics.notify(this.sharedStdin, 0);
            for (let x = 0; x < newStdinData.length; ++x) {
                stdinBuffer[x] = newStdinData[x];
            }

            // Return the current std
            return newStdinData.length;
        } else {
            console.warn("Should not be here");
            return 0;
        }
    }
}

function onWriteStdout(stdout, process)
{
    let str = stdout.toString();
    
    if (str === "\n")
        return process.lastReq;

    postMessage(str);

    let req = str.split(" ")[0];
    return req === "REQ";
}


function onReadStdin()
{
}

let sharedBuffer = null;

function wait(time)
{
    return new Promise((res)=>setTimeout(res, time));
}

onmessage = (e) => {
    sharedBuffer = e.data;
}

(async function mainWorker() {
    while (!sharedBuffer)
    {
        await wait(100);
    }

    let process = new Process("qjs.wasm", sharedBuffer, onWriteStdout, onReadStdin);
    await process.start();
})();
