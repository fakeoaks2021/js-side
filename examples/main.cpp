#include <iostream>
#include <vector>
#include <map>

using namespace std;
using ll = long long;

vector<vector<ll>> bin(1000, vector<ll>(1000, -1));

ll calc_bin(ll n, ll k)
{
    if (k == 0 || n == k) return 1;
    if (bin[n][k] != -1) return bin[n][k];

    bin[n][k] = calc_bin(n - 1, k - 1) + calc_bin(n - 1, k);
    return bin[n][k];
}

int main()
{
    map<string, function<double(double, double)>> operations;
    operations["add"] = ([](double a, double b) {
        return a + b;
    });

    operations["sub"] = ([](double a, double b) {
            return a - b;
    });

    operations["mult"] = ([](double a, double b) {
        return a * b;
    });

    operations["div"] = ([](double a, double b) {
        return a / b;
    });

    operations["prime"] = ([](double a, double b) {
        ll currp = 2;
        ll primetg = a;
        ll curr = 1;

        while (primetg != curr)
        {
            ++currp;
            bool isPrime = true;

            for (ll j = 2; j * j <= currp; ++j)
            {
                if (currp % j == 0)
                {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime)
                ++curr;
        }

        return currp;
    });

    operations["bin"] = ([](double a, double b) {
        return calc_bin((ll)a, (ll)b);
    });

    for (const pair<string, function<double(double, double)>>& op : operations )
    {
        const string buttonid = op.first;

        cout << "LST " << buttonid << " click" << endl;
    }

    string name, id, event;

    while (cin >> name >> id >> event)
    {
        if (name == "EVT" && event == "click")
        {
            double a, b;
            string s;

            cout << "REQ i1 value" << endl;
            cin >> s >> s >> s >> a;
            cout << "REQ i2 value" << endl;
            cin >> s >> s >> s >> b;

            double res = operations[id](a, b);
            cout << "FILL res " << res << endl;
        }
    }

    return 0;
}
