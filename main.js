import 'regenerator-runtime/runtime'

const worker = new Worker('process.js');
let buff = new SharedArrayBuffer(1024);
let arr = new Int32Array(buff);
let messageQueue = [];
let waiting = false;

let request_element = "";
let request_attr =  "";
let request_val = "";

Atomics.store(arr, 0, -1);
Atomics.notify(arr, 0);

function copyToArray(message)
{
    for (let i = 0; i < message.length; ++i)
        arr[i + 1] = message.charCodeAt(i);

    Atomics.store(arr, 0, message.length);
    Atomics.notify(arr, 0);
}

function copyMessageToArray()
{
    if (messageQueue.length === 0)
    {
        waiting = true;
        return;
    }
    
    let message = messageQueue.shift();
    copyToArray(message);
}

function sendToCpp(message)
{
    messageQueue.push(message);

    if (waiting)
    {
        waiting = false;
        let m = messageQueue.shift();

        copyToArray(m);
    }
}

worker.postMessage(buff);

function executeCommand(command)
{
    let commandList = command.split(" ");
    let id = commandList[1];

    switch (commandList[0])
    {
    case "REQ":
        serveRequest(id, commandList[2]);
    break;

    case "FILL":
        fillElement(id, commandList[2]);
    break;

    case "LST":
        setEventListeners(id, commandList[2]);
    break;

    case "CHG":
        changeAttr(id, commandList[2], commandList[3]);
    break;
    }
}

function serveRequest(id, attr)
{
    request_element = id;
    request_attr = attr;

    let elem = document.getElementById(id);

    if (attr === "value") request_val = elem.value;
    else if (attr === "innerHTML") request_val = elem.innerHTML;
    else request_val = elem.getAttribute(attr);
}

function fillElement(id, val)
{
    let elem = document.getElementById(id);
    elem.innerHTML = val;
}

function genericEventHandler(event)
{
    let command = "EVT " + event.target.id + " " + event.type + "\n";
    sendToCpp(command);
}

function setEventListeners(id, event)
{
    document.getElementById(id).addEventListener(event, genericEventHandler);
}

function changeAttr(id, attr, val)
{
    let elem = document.getElementById(id);
    if (attr === "value") elem.value = val;
    else elem.setAttribute(attr, val);
}

worker.onmessage = (e) =>
{
    let command = e.data;
    console.log("Received: " + command);
    
    if (command === "answer")
    {
        // answer a user request
        copyToArray("ANS " + request_element + " " + request_attr + " " + request_val + "\n");
    }
    else if (command === "notify")
    {
        copyMessageToArray();
    }
    else
    {
        // Aqui es llegeixen les comandes que s'envien des del C++
        executeCommand(command);
    }
}
